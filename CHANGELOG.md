# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2022-xx-xx

### Added

- Upgrade Calculator - main functionality of this release which provides calculation of upgrading costs and other requirements for each card type (based on current requirements from Smashing Four WIKI),
- Tech stack - page which contains list of used programming languages/frameworks/other stuffs, grouped by architecture layer.
