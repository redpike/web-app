FROM node:16.18.1-alpine as build-stage

WORKDIR /app

COPY package*.json /app/

RUN npm install

COPY ./ /app/

RUN npm run build

FROM nginx:1.23.2-alpine

WORKDIR /usr/share/nginx/html

COPY --from=build-stage /app/build .

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]
