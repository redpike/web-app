export enum CardType {
    COMMON = 'COMMON',
    RARE = 'RARE',
    EPIC = 'EPIC'
}
