import TechStackBox from '../../feature/techstack/components/TechStackBox/TechStackBox';

const TechStack = () => {
    return (
        <div className={'container'}>
            <div className={'upgrade__form bg-slate-100 rounded-xl p-8 dark:bg-slate-800'}>
                <div className={'header text-lg font-semibold'}>
                    <h2>Tech Stack</h2>
                </div>
                <div className={'description'}>
                    <p>Technology stack used in the project.</p>
                </div>
                <TechStackBox></TechStackBox>
            </div>
        </div>
    );
};

export default TechStack;
