import UpgradeForm from '../../feature/upgrade/components/UpgradeForm/UpgradeForm';
import UpgradeResult from '../../feature/upgrade/components/UpgradeResult/UpgradeResult';
import {FunctionComponent, useState} from 'react';
import {UpgradeResponse} from '../../feature/upgrade/models/upgradeTypes';
import './Upgrade.scss';

const UpgradeComponent: FunctionComponent = () => {

    const [data, setUpgradeData] = useState<UpgradeResponse>();

    return (
        <div className={'container'}>
            <div className={'upgrade__form bg-slate-100 rounded-xl p-8 dark:bg-slate-800'}>
                <div className={'header text-lg font-semibold'}>
                    <h2>Upgrade Calculator</h2>
                </div>
                <div className={'description'}>
                    <p>The calculator is used to calculate the potential promotion of a character, giving the current level, the number of cards and rarity of card.</p>
                    <p>To get the result, click the Calculate button or press ENTER.</p>
                </div>
                <UpgradeForm refresh={setUpgradeData}></UpgradeForm>
                <UpgradeResult {...data}></UpgradeResult>
            </div>
        </div>
    );
}

export default UpgradeComponent;
