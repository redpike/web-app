const {createProxyMiddleware} = require('http-proxy-middleware');

module.exports = function (app) {
    app.use(
        '/api/sfh/v1/*',
        createProxyMiddleware({
            target: 'http://127.0.0.1:9200',
            changeOrigin: true,
            secure: false,
            logLevel: 'debug'
        })
    );
};
