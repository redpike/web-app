import {ApplicationType, SwitcherProps} from '../../models/techstackTypes';
import {useState} from 'react';
import './Switcher.scss';

const Switcher = (props: SwitcherProps) => {

    const [type, setSelectedType] = useState<ApplicationType>('web');

    const prepareCssClasses = (applicationType: ApplicationType): string => {
        let classes = 'switch-button';
        if (type === applicationType) {
            classes = 'switch-button active';
        }
        return classes;
    }

    const switchLayer = (type: ApplicationType): void => {
        props.onChange(type);
        setSelectedType(type);
    }

    return (
        <div className={'switcher'}>
            <div className={prepareCssClasses('web')}>
                <button onClick={() => switchLayer('web')}>Web</button>
            </div>
            <div className={prepareCssClasses('api')}>
                <button onClick={() => switchLayer('api')}>API</button>
            </div>
            <div className={prepareCssClasses('scraper')}>
                <button onClick={() => switchLayer('scraper')}>Scraper</button>
            </div>
        </div>
    );
};

export default Switcher;
