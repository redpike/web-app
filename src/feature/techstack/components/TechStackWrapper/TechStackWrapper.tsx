import techstackData from '../../techstack.json';
import {TechStackItemDetails, TechStackWrapperProps} from '../../models/techstackTypes';
import TechStackItem from '../TechStackItem/TechStackItem';
import './TechStackWrapper.scss';

const TechStackWrapper = (props: TechStackWrapperProps) => {

    return (
        <div className={'tech-stack-list'}>
            {
                techstackData.filter(entry => entry.type === props.selectedType)
                    .map((item) => item.items.map((entry: TechStackItemDetails) => {
                        return <TechStackItem key={entry.class}
                                              name={entry.name}
                                              class={entry.class}
                                              description={entry.description}></TechStackItem>
                    }))
            }
        </div>
    );
};

export default TechStackWrapper;
