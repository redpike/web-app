import {TechStackItemDetails} from '../../models/techstackTypes';
import './TechStackItem.scss';

const TechStackItem = (item: TechStackItemDetails) => {

    return (
        <div className={'tech-stack-item'}>
            <div className={'tech-stack-item--icon ' + item.class}></div>
            <div className={'tech-stack-item--name'}>{item.name}</div>
            <div className={'tech-stack-item--description'}>{item.description}</div>
        </div>
    );
};

export default TechStackItem;
