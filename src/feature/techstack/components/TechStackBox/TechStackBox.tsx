import Switcher from '../Switcher/Switcher';
import {useState} from 'react';
import {ApplicationType} from '../../models/techstackTypes';
import TechStackWrapper from '../TechStackWrapper/TechStackWrapper';

const TechStackBox = () => {

    const [type, setSelectedType] = useState<ApplicationType>('web');

    return (
        <div className={'tech-stack-box'}>
            <Switcher onChange={setSelectedType}></Switcher>
            <TechStackWrapper selectedType={type}></TechStackWrapper>
        </div>
    );
};

export default TechStackBox;
