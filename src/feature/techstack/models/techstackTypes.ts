export interface TechStackItemDetails {
    name: string;
    class: string;
    description: string;
}

export declare type ApplicationType = 'web' | 'api' | 'scraper';

export interface SwitcherProps {
    onChange: (type: ApplicationType) => void;
}

export interface TechStackWrapperProps {
    selectedType: ApplicationType;
}
