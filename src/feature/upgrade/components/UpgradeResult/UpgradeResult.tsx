import {Component, ReactNode} from 'react';
import {UpgradeResponse} from '../../models/upgradeTypes';
import './UpgradeResult.scss';

class UpgradeResult extends Component<any, UpgradeResponse> {

    render(): ReactNode {
        return (
            <div className={'upgrade__result'}>
                <div className={'result__field text-gray-700 text-sm font-bold mb-2'}>
                    <div className={'label'}>
                        Possible level
                    </div>
                    <div className={'value'}>
                        {this.props?.possibleLevel}
                    </div>
                </div>
                <div className={'result__field text-gray-700 text-sm font-bold mb-2'}>
                    <div className={'label'}>
                        Cards
                    </div>
                    <div className={'value'}>
                        {this.props?.cardsLeft} {this.props.possibleLevel ? '/' : ''} {this.props?.cardsToGo}
                    </div>
                </div>
                <div className={'result__field text-gray-700 text-sm font-bold mb-2'}>
                    <div className={'label'}>
                        Gold
                    </div>
                    <div className={'value'}>
                        {this.props?.gold}
                    </div>
                </div>
            </div>
        )
    }
}

export default UpgradeResult;
