import {CardType} from '../../../../shared/models/cardType';
import upgradeService from '../../services/upgradeService';
import {UpgradeProps, UpgradeResponse} from '../../models/upgradeTypes';
import {SubmitHandler, useForm} from 'react-hook-form';
import './UpgradeForm.scss';

type State = {
    currentLevel: string;
    cards: string;
    cardType: CardType
}

const UpgradeForm = (props: UpgradeProps) => {

    const {register, getValues, setValue, handleSubmit, formState} = useForm<State>();

    const onSubmit: SubmitHandler<State> = data => {
        if (formState.isValid) {
            const response: UpgradeResponse = upgradeService.getCalculationFromJson(data.currentLevel, data.cards, data.cardType);
            if (props?.refresh) {
                props.refresh(response);
            }
        }
    };

    const validateInputs = (field: any): void => {
        const value: number = parseInt(getValues(field) ?? '0');
        let maxValue: number;

        switch (field) {
            case 'cards': {
                const cardType: CardType = getValues('cardType');
                maxValue = maxCardsAmount(cardType);
                break;
            }
            case 'currentLevel':
            default: {
                maxValue = parseInt(process.env.REACT_APP_GAME_MAX_LEVEL ?? '0', 10);
                break;
            }
        }

        if (value > maxValue) {
            setValue(field, maxValue.toFixed());
        }
    };

    const maxCardsAmount = (cardType: CardType): number => {
        let maxAmount: number;
        switch (cardType) {
            case CardType.RARE: {
                const paramValue: string = process.env.REACT_APP_GAME_MAX_CARDS_RARE ?? '0';
                maxAmount = parseInt(paramValue, 10);
                break;
            }
            case CardType.EPIC: {
                const paramValue: string = process.env.REACT_APP_GAME_MAX_CARDS_EPIC ?? '0';
                maxAmount = parseInt(paramValue, 10);
                break;
            }
            default: {
                const paramValue: string = process.env.REACT_APP_GAME_MAX_CARDS_COMMON ?? '0';
                maxAmount = parseInt(paramValue, 10);
                break;
            }
        }
        return maxAmount;
    }

    return (
        <div className={'form__wrapper'}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className={'form-control'}>
                    <label htmlFor={'currentLevel'} className={'text-gray-700 text-sm font-bold mb-2'}>Current
                        level</label>
                    <input type={'number'}
                           className={'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'}
                           min={1}
                           max={process.env.REACT_APP_GAME_MAX_LEVEL}
                           defaultValue={1}
                           {...register('currentLevel', {
                                   min: {
                                       value: 1,
                                       message: 'Minimum level is 1'
                                   },
                                   max: {
                                       value: parseInt(process.env.REACT_APP_GAME_MAX_LEVEL ?? '0', 10),
                                       message: `Maximum level is ${process.env.REACT_APP_GAME_MAX_LEVEL}`
                                   },
                                   pattern: {
                                       value: /^\d*$/,
                                       message: 'Only digits are acceptable'
                                   },
                                   onBlur: () => validateInputs('currentLevel')
                               }
                           )}/>
                </div>
                <div className={'form-control'}>
                    <label htmlFor={'cards'} className={'text-gray-700 text-sm font-bold mb-2'}>Cards</label>
                    <input type={'number'}
                           className={'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'}
                           min={0}
                           max={maxCardsAmount(getValues('cardType'))}
                           defaultValue={0}
                           {...register('cards', {
                                   pattern: {
                                       value: /^\d*$/,
                                       message: 'Only digits are acceptable'
                                   },
                                   onBlur: () => validateInputs('cards')
                               }
                           )}/>
                </div>
                <div className={'form-control inline-block relative w-64'}>
                    <label htmlFor={'cardType'} className={'text-gray-700 text-sm font-bold mb-2'}>Rarity</label>
                    <select
                        className={'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'}
                        {...register('cardType')}>
                        <option value={CardType.COMMON}>Common</option>
                        <option value={CardType.RARE}>Rare</option>
                        <option value={CardType.EPIC}>Epic</option>
                    </select>
                    <div
                        className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                        </svg>
                    </div>
                </div>
                <div className={'submit'}>
                    <button
                        className={'flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded'}
                        disabled={!formState?.isValid}
                        type={'submit'}>Calculate
                    </button>
                </div>
            </form>
        </div>
    );
};

export default UpgradeForm;
