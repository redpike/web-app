import {CardType} from '../../../shared/models/cardType';

export interface UpgradeData {
    [key: string]: UpgradeDataEntry;
}

export interface UpgradeDataEntry {
    level: string;
    gold: string;
    cards: string;
}

export interface UpgradeRequest {
    currentLevel: string;
    cards: string;
    cardType: CardType
}

export interface UpgradeResponse {
    possibleLevel: string;
    cardsLeft: string;
    cardsToGo: string;
    gold: string
}

export interface UpgradeProps {
    refresh?: (data: UpgradeResponse) => void;
}
