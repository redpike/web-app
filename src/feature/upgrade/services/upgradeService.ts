import {AxiosResponse} from 'axios';
import http from '../../../hooks/httpCommon';
import {UpgradeData, UpgradeDataEntry, UpgradeRequest, UpgradeResponse} from '../models/upgradeTypes';
import {CardType} from '../../../shared/models/cardType';
import upgradesData from '../upgrades.json';

class UpgradeService {

    getCalculation(currentLevel: string, cards: string, cardType: CardType): Promise<AxiosResponse<UpgradeResponse>> {
        const requestData: UpgradeRequest = {currentLevel, cards, cardType};
        return http.put<UpgradeResponse>('/api/sfh/v1/upgrades', requestData)
    }

    getCalculationFromJson(currentLevel: string, cards: string, cardType: CardType): UpgradeResponse {
        const filteredByLevel = this.filterEntriesByTypeAndLevel(cardType, currentLevel);

        let possibleLevel: number = parseInt(currentLevel);
        let cardsLeft: number = parseInt(cards);
        let nextLevelCards: string = '0';
        let gold: number = 0;

        for (const value of filteredByLevel.values()) {
            nextLevelCards = value.cards;
            const cardsInt: number = parseInt(value.cards);
            if (cardsLeft < cardsInt || possibleLevel === parseInt(process.env.REACT_APP_GAME_MAX_LEVEL ?? '0')) {
                break;
            }
            possibleLevel += 1;
            cardsLeft -= cardsInt;
            const cost: number = parseInt(value.gold);
            gold += cost;
        }

        return {
            possibleLevel: possibleLevel.toString(),
            cardsLeft: cardsLeft.toString(),
            cardsToGo: nextLevelCards,
            gold: gold.toString()
        };
    }

    private filterEntriesByTypeAndLevel(cardType: CardType, currentLevel: string) {
        const filteredByCardType: UpgradeData = upgradesData.entries[cardType] as UpgradeData;

        const filteredByLevel: Map<string, UpgradeDataEntry> = new Map<string, UpgradeDataEntry>();
        for (const [key, value] of Object.entries(filteredByCardType)) {
            if (parseInt(key) >= parseInt(currentLevel)) {
                filteredByLevel.set(key, value);
            }
        }
        return filteredByLevel;
    }
}

export default new UpgradeService();
