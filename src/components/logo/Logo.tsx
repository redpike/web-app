import './Logo.scss';

const Logo = () => {

    return (
        <div className={'logo-wrapper'}>
            <div className={'logo'}></div>
            <div className={'name'}>Smashing Four Handbook</div>
        </div>
    );
}

export default Logo;
