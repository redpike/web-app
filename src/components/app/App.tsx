import './App.scss';
import {Route, Routes} from 'react-router-dom';
import Upgrade from '../../routes/upgrade/Upgrade';
import NotFound from '../../routes/not-found/NotFound';
import TechStack from '../../routes/techstack/TechStack';
import Footer from '../footer/Footer';
import Navbar from '../navbar/Navbar';

const App = () => {
    return (
        <div className="App">
            <Navbar>
                <Routes>
                    <Route path='/' element={<Upgrade/>}/>
                    <Route path='/calculator' element={<Upgrade/>}/>
                    <Route path='/techstack' element={<TechStack/>}/>
                    <Route path='*' element={<NotFound/>}/>
                </Routes>
            </Navbar>
            <Footer />
        </div>
    );
};

export default App;
