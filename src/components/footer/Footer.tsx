import './Footer.scss';
import Container from '../contrainer/Container';

const Footer = () => {

    return (
        <footer>
            <Container>
                <div className={'footer'}>
                    <div className={'left'}>
                        Code Beat © 2022
                    </div>
                    <div className={'right'}>
                        All rights reserved
                    </div>
                </div>
            </Container>
        </footer>
    );
};

export default Footer;
