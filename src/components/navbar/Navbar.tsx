import {BrowserRouter, Link} from 'react-router-dom';
import Container from '../contrainer/Container';
import './Navbar.scss';
import Logo from '../logo/Logo';
import {useEffect, useState} from 'react';

const useWindowSize = (): boolean => {
    const [isMobile, setIsMobile] = useState(false);

    useEffect(() => {
        const handleResize = (): void => {
            const result: boolean = window.innerWidth <= 800;
            setIsMobile(result);
        }
        window.addEventListener("resize", handleResize);
        handleResize();
        return () => window.removeEventListener("resize", handleResize);
    }, []); // Empty array ensures that effect is only run on mount
    return isMobile;
}

const Navbar = (props: any) => {

    const isMobile: boolean = useWindowSize();

    return (
        <BrowserRouter>
            <div className={'nav'}>
                <Container>
                    <div className={'left'}>
                        <Logo />
                    </div>
                    <div className={'right'}>
                        <nav>
                            <ul>
                                <li>
                                    <Link to='/calculator'>Upgrade Calculator</Link>
                                </li>
                                <li>
                                    <Link to='/techstack'>Tech Stack</Link>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </Container>
            </div>
            <div className={'content'}>
                <Container>
                    {props.children}
                </Container>
            </div>
        </BrowserRouter>
    );
};

export default Navbar;
